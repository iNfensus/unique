<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'addnew' => 'Add new post',
    'edit' => 'Edit post',
    'sn' => 'SN',
    'title' => 'Title',
    'desc' => 'Description',
    'image' => 'Featured image',
    'link' => 'Link',
    'category' => 'Category',
    'status' => 'Status',
    'action' => 'Action',
    'save' => 'Save',

];
