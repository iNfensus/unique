<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'categories' => 'Active categories',
    'services' => 'Active services',
    'portfolio' => 'Active portfolio',
    'teams' => 'Active team members',
    'admin' => 'Admin panel',
    'logged' => 'You are logged in',
    'index' => 'Show website',
    'logout' => 'Logout',
];
