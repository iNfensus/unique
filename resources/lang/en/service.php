<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'addnew' => 'Add new service',
    'title' => 'Title',
    'desc' => 'Description',
    'icon' => 'Icon',
    'status' => 'Status',
    'sn' => 'SN',
    'description' => 'Description',
    'action' => 'Action',
    'edit' => 'Edit service',
    'save' => 'Save',

];
