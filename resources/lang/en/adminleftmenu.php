<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'dashboard' => 'Dashboard',
    'settings' => 'Settings',
    'categories' => 'Categories',
    'content' => 'Content',
    'allposts' => 'All posts',
    'editpost' => 'Edit post',
    'editcategory' => 'Edit category',
    'addcategory' => 'Categories',
    'addnewpost' => 'Add new post',
    'addnew' => 'Add new posts',
    'backups' => 'Backups',
    'home' => 'Home',
    'setup' => 'Settings',
    'addnewservice' => 'Add new service',
    'service' => 'Services',
    'allservice' => 'All services',
    'allservices' => 'All services',
    'portfolio' => 'Portfolio',
    'allportfolio' => 'All portfolio',
    'allportfolios' => 'All portfolios',
    'addnewportfolio' => 'Add new portfolio',
    'portcats' => 'Portfolio categories',
    'addportcats' => 'Add new portfolio category',
    'editportfolio' => 'Edit portfolio',
    'editportfoliocategory' => 'Edit portfolio category',
    'clients' => 'Clients',
    'editclient' => 'Edit client',
    'editservice' => 'Edit service',
    'team' => 'Team',
    'allmembers' => 'All members',
    'newmember' => 'Add new member',
    'editeam' => 'Edit single member',
    'addnewmember' => 'Add new member',

];
