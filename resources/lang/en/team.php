<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'sn' => 'SN',
    'name' => 'Name',
    'designation' => 'Designation',
    'image' => 'Image',
    'intro' => 'Intro',
    'social' => 'Social media',
    'status' => 'Status',
    'action' => 'Action',
    'edit' => 'Edit',
    'save' => 'Save',
    'addnew' => 'Add new member',
    'profileimage' => 'Add profile image',

];
