<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'backups' => 'Backups Manager',
    'home' => 'Home',
    'dashboard' => 'Dashboard',
    'allposts' => 'All posts',
    'editpost' => 'Edit post',
    'editcategory' => 'Edit category',
    'addcategory' => 'Add new category',
    'addnewpost' => 'Add new post',
    'setup' => 'Settings',
    'categories' => 'Categories',
    'addnewservice' => 'Add new service',
    'allservices' => 'All services',
    'editservice' => 'Edit service',
    'allportfolio' => 'All portfolio',
    'editportfolio' => 'Edit portfolio',
    'addnewportfolio' => 'Add new portfolio',
    'portcats' => 'Portfolio category',
    'editportfoliocategory' => 'Edit portfolio category',
    'clients' => 'Clients',
    'editclient' => 'Edit client',
    'nodata' => 'Theres no data to display',
    'allmembers' => 'All members',
    'editeam' => 'Edit single member',
    'addnewmember' => 'Add new member',
    'calendar' => 'Calendar',
];
