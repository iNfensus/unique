<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'settings' => 'Settings',
    'upload' => 'Upload image',
    'sitetitle' => 'Website title',
    'adress' => 'Adress',
    'phone' => 'Phone number',
    'email' => 'E-mail',
    'social' => 'Social Media',
    'sorry' => 'Sorry!',
    'warning' => 'You have reached maxium number of fields for social profile links.',
    'save' => 'Save settings',
    'keywords' => 'Meta keywords',
    'description' => 'Meta description',
    'seo' => 'SEO Settings',
    'gacode' => 'Google Analytics Tracking Code',

];
