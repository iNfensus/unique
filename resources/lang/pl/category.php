<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'categories' => 'Kategorie',
    'name' => 'Nazwa kategorii',
    'status' => 'Status',
    'on' => 'On',
    'off' => 'Off',
    'addnew' => 'Dodaj nową',
    'sn' => 'SN',
    'action' => 'Akcja',
    'edit' => 'Edytuj kategorie',
    'save' => 'Zapisz',

];
