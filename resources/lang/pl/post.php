<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'addnew' => 'Dodaj nowy post',
    'edit' => 'Edytuj post',
    'sn' => 'SN',
    'title' => 'Tytuł',
    'desc' => 'Opis',
    'image' => 'Wyróżniony obrazek',
    'link' => 'Link',
    'category' => 'Kategoria',
    'status' => 'Status',
    'action' => 'Akcja',
    'save' => 'Zapisz',

];
