<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'sn' => 'SN',
    'title' => 'Tytuł',
    'image' => 'Obrazek',
    'category' => 'Kategoria',
    'status' => 'Status',
    'action' => 'Akcja',
    'edit' => 'Edycja',
    'save' => 'Zapisz',
    'addnew' => 'Dodaj nowe',
    'name' => 'Tytuł',

];
