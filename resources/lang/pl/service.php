<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'addnew' => 'Dodaj nową usługę',
    'title' => 'Tytuł',
    'desc' => 'Opis',
    'icon' => 'Ikona',
    'status' => 'Status',
    'edit' => 'Edytuj usługę',
    'save' => 'Zapisz',

];
