<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Backup Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the backup system.
    | You are free to change them to anything you want to customize your views to better match your application.
    |
    */
    'backup'                         => 'Kopia zapasowa',
    'create_a_new_backup'            => 'Utwórz nową kopie zapasową',
    'existing_backups'               => 'Istniejące kopie zapasowe',
    'date'                           => 'Data',
    'file_size'                      => 'Rozmiar pliku',
    'actions'                        => 'Akcja',
    'download'                       => 'Pobierz',
    'delete'                         => 'Usuń',
    'delete_confirm'                 => 'Czy na pewno chcesz usunąć ten plik kopii zapasowej?',
    'delete_confirmation_title'      => 'Wykonane',
    'delete_confirmation_message'    => 'Plik kopii zapasowej został usunięty.',
    'delete_error_title'             => 'Błąd',
    'delete_error_message'           => 'Plik kopii zapasowej NIE ZOSTAL usunięty.',
    'delete_cancel_title'            => "W porządku",
    'delete_cancel_message'          => 'Plik kopii zapasowej NIE został usunięty.',
    'create_confirmation_title'      => 'Rozpoczął się proces tworzenia kopii zapasowej.',
    'create_confirmation_message'    => 'Proszę poczekać kilka minut, strona odświeży się automatycznie!',
    'create_error_title'             => 'Błąd kopii zpasowej',
    'create_error_message'           => 'Nie można utworzyć pliku kopii zapasowej.',
    'create_warning_title'           => 'Nieznany błąd',
    'create_warning_message'         => 'Twoja kopia zapasowa NIE mogła zostać utworzona. Sprawdź logi systemowe.',
    'location'                       => 'Lokalizacja',
    'no_disks_configured'            => 'Brak skonfigurowanych dysków w pliku config/backup.php',
    'backup_doesnt_exist'            => "Plik kopii zapasowej nie istnieje!.",
    'only_local_downloads_supported' => 'Obsługiwane są tylko pliki do pobrania z lokalnego systemu plików.',
];
