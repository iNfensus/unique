<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'sn' => 'SN',
    'name' => 'Imię',
    'designation' => 'Stanowisko',
    'image' => 'Zdjęcie',
    'intro' => 'Opis',
    'social' => 'Social media',
    'status' => 'Status',
    'action' => 'Akcja',
    'edit' => 'Edytuj',
    'save' => 'Zapisz',
    'addnew' => 'Dodaj nowego pracownika',
    'profileimage' => 'Dodaj zdjęcie profilowe',

];
