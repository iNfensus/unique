<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'dashboard' => 'Pulpit',
    'settings' => 'Ustawienia',
    'categories' => 'Kategorie',
    'content' => 'Treść',
    'allposts' => 'Wszystkie posty',
    'editpost' => 'Edytuj post',
    'editcategory' => 'Edytuj kategorie',
    'addcategory' => 'Kategorie',
    'addnewpost' => 'Dodaj nowy post',
    'addnew' => 'Dodaj nowy post',
    'backups' => 'Kopie zapasowe',
    'home' => 'Home',
    'setup' => 'Ustawienia',
    'addnewservice' => 'Dodaj nową usługę',
    'service' => 'Usługi',
    'allservice' => 'Wszystkie usługi',
    'allservices' => 'Wszystkie usługi',
    'portfolio' => 'Portfolio',
    'allportfolio' => 'Wszystkie portfolia',
    'allportfolios' => 'Wszystkie portfolia',
    'addnewportfolio' => 'Dodaj nowe portfolio',
    'portcats' => 'Kategorie portfolio',
    'addportcats' => 'Dodaj nową kategorie portfolio',
    'editportfolio' => 'Edytuj portfolio',
    'editportfoliocategory' => 'Edytuj kategorie portfolio',
    'clients' => 'Klienci',
    'editclient' => 'Edytuj klienta',
    'editservice' => 'Edytuj usługę',
    'team' => 'Zespół',
    'allmembers' => 'Wszyscy pracownicy',
    'newmember' => 'Dodaj nowego pracownika',
    'editeam' => 'Edytuj pracownika',
    'addnewmember' => 'Dodaj nowego pracownika',

];
