<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'settings' => 'Ustawienia',
    'upload' => 'Wgraj logo',
    'sitetitle' => 'Tytuł strony',
    'adress' => 'Adres',
    'phone' => 'Numer telefonu',
    'email' => 'Adres e-mail',
    'social' => 'Social Media',
    'sorry' => 'Przepraszamy!',
    'warning' => 'Osiągnięto maksymalną liczbę pól dla linków do profilu społecznościowego.',
    'save' => 'Zapisz ustawienia',
    'keywords' => 'Meta keywords',
    'description' => 'Meta description',
    'seo' => 'Ustawienia SEO',
    'gacode' => 'Google Analytics Tracking Code',

];
