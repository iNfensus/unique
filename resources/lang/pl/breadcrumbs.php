<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'backups' => 'Menadżer kopii zapasowych',
    'home' => 'Home',
    'dashboard' => 'Pulpit',
    'allposts' => 'Wszystkie posty',
    'editpost' => 'Edytuj post',
    'editcategory' => 'Edytuj kategorie',
    'addcategory' => 'Dodaj nową kategorie',
    'addnewpost' => 'Dodaj nowy post',
    'setup' => 'Ustawienia',
    'categories' => 'Kategorie',
    'addnewservice' => 'Dodaj nową usługę',
    'allservices' => 'Wszystkie usługi',
    'editservice' => 'Edytuj usługę',
    'allportfolio' => 'Wszystkie portfolio',
    'editportfolio' => 'Edytuj portfolio',
    'addnewportfolio' => 'Dodaj nowe portfolio',
    'portcats' => 'Kategorie portfolio',
    'editportfoliocategory' => 'Edytuj kategorie portfolio',
    'clients' => 'Klienci',
    'editclient' => 'Edytuj klienta',
    'nodata' => 'Brak danych do wyświetlenia',
    'allmembers' => 'Wszyscy pracownicy',
    'editeam' => 'Edytuj pracownika',
    'addnewmember' => 'Dodaj nowego pracownika',
    'calendar' => 'Kalendarz',
];
