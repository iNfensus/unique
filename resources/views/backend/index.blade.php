    @extends('backend.master')
    @section('content')

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('admin')}}" class="brand-link">
      <img src="{{url('public/backend/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">{{ trans('admin.admin') }}</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{url('public/backend/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{url('admin')}}" class="nav-link active">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            {{ trans('adminleftmenu.dashboard') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{url('setups')}}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            {{ trans('adminleftmenu.settings') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{url('categories')}}" class="nav-link">
                        <i class="nav-icon fas fa-copy"></i>
                        <p>
                            {{ trans('adminleftmenu.categories') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="" class="nav-link">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            {{ trans('adminleftmenu.content') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview" style="display: none;">
                        <li class="nav-item">
                            <a href="{{url('all-posts')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.allposts') }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('new-post')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.addnew') }}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            {{ trans('adminleftmenu.service') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('all-services')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.allservice') }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('new-service')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.addnewservice') }}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            {{ trans('adminleftmenu.portfolio') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('all-portfolio')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.allportfolios') }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('new-portfolio')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.addnewportfolio') }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('portcats')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.portcats') }}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{url('clients')}}" class="nav-link">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            {{ trans('adminleftmenu.clients') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            {{ trans('adminleftmenu.team') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('all-members')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.allmembers') }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('new-member')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.newmember') }}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{url('backup')}}" class="nav-link">
                        <i class="nav-icon fas fa-folder"></i>
                        <p>
                            {{ trans('adminleftmenu.backups') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>


<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ trans('breadcrumbs.dashboard') }}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
              <div class="card-body">
                  @if (session('status'))
                      <div class="alert alert-success" role="alert">
                          {{ session('status') }}
                      </div>
                  @endif

                      {{ trans('admin.logged') }}
              </div>
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('admin')}}">{{ trans('breadcrumbs.home') }}</a></li>
              <li class="breadcrumb-item active">{{ trans('adminleftmenu.dashboard') }}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            @if(isset($cats))
          <div class="col-lg-3 col-6">
            <!-- small box -->
              <div class="info-box">
                  <span class="info-box-icon bg-info"><i class="far fa-folder"></i></span>

                  <div class="info-box-content">
                      <span class="info-box-text">{{ trans('admin.categories') }}</span>
                      <span class="info-box-number">{{$cats->count()}}</span>
                  </div>
                  <!-- /.info-box-content -->
              </div>
          </div>
        @else
            <!--Empty Categories if no data or data status is off-->
        @endif
          <!-- ./col -->
        @if(isset($services))
          <div class="col-lg-3 col-6">
            <!-- small box -->
              <div class="info-box">
                  <span class="info-box-icon bg-success"><i class="far fa-bookmark"></i></span>

                  <div class="info-box-content">
                      <span class="info-box-text">{{ trans('admin.services') }}</span>
                      <span class="info-box-number">{{$services->count()}}</span>
                  </div>
                  <!-- /.info-box-content -->
              </div>
          </div>
            @else
                <!--Empty Services if no data or data status is off-->
            @endif
          <!-- ./col -->
         @if(isset($portfolio))
          <div class="col-lg-3 col-6">
            <!-- small box -->
              <div class="info-box">
                  <span class="info-box-icon bg-warning"><i class="fab fa-medapps"></i></span>

                  <div class="info-box-content">
                      <span class="info-box-text">{{ trans('admin.portfolio') }}</span>
                      <span class="info-box-number">{{$portfolio->count()}}</span>
                  </div>
                  <!-- /.info-box-content -->
              </div>
          </div>
            @else
                <!--Empty Portfolio if no data or data status is off-->
            @endif
          <!-- ./col -->
         @if(isset($teams))
          <div class="col-lg-3 col-6">
            <!-- small box -->
              <div class="info-box">
                  <span class="info-box-icon bg-danger"><i class="fas fa-users"></i></span>

                  <div class="info-box-content">
                      <span class="info-box-text">{{ trans('admin.teams') }}</span>
                      <span class="info-box-number">{{$teams->count()}}</span>
                  </div>
                  <!-- /.info-box-content -->
              </div>
          </div>
            @else
                <!--Empty Teams if no data or data status is off-->
            @endif
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
              <!-- TO DO List -->
        @include('backend.features.task')
              <!-- /TO DO List -->
          <!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
        @include('backend.features.calendar')
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>

    @stop
