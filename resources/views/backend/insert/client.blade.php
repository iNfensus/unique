@extends('backend.master')
@section('content')

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('admin')}}" class="brand-link">
      <img src="{{url('public/backend/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{url('public/backend/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{url('admin')}}" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            {{ trans('adminleftmenu.dashboard') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{url('setups')}}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            {{ trans('adminleftmenu.settings') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{url('categories')}}" class="nav-link">
                        <i class="nav-icon fas fa-copy"></i>
                        <p>
                            {{ trans('adminleftmenu.categories') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            {{ trans('adminleftmenu.content') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('all-posts')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.allposts') }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('new-post')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.addnew') }}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            {{ trans('adminleftmenu.service') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('all-services')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.allservice') }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('new-service')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.addnewservice') }}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            {{ trans('adminleftmenu.portfolio') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('all-portfolio')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.allportfolios') }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('new-portfolio')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.addnewportfolio') }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('portcats')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.portcats') }}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{url('clients')}}" class="nav-link active">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            {{ trans('adminleftmenu.clients') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            {{ trans('adminleftmenu.team') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('all-members')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.allmembers') }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('new-member')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.newmember') }}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{url('backup')}}" class="nav-link">
                        <i class="nav-icon fas fa-folder"></i>
                        <p>
                            {{ trans('adminleftmenu.backups') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <div class="content-header">
      <div class="container-fluid">
          <div class="row mb-2">
              <div class="col-sm-6">
                  <h1 class="m-0 text-dark">{{ trans('breadcrumbs.clients') }}</h1>
              </div><!-- /.col -->
              <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                      <li class="breadcrumb-item"><a href="{{url('admin')}}">{{ trans('breadcrumbs.home') }}</a></li>
                      <li class="breadcrumb-item active">{{ trans('adminleftmenu.clients') }}</li>
                  </ol>
              </div><!-- /.col -->
          </div><!-- /.row -->
      </div><!-- /.container-fluid -->
  </div>

     <section class="content">
        <div class="container-fluid">
            <div class="row">
                @if(Session::has('message'))
                <div class="col-sm-12">
                    <div class="alert alert-success alert-dismissable">
                        {{ session::get('message') }}
                        <a class="close" data-dismiss="alert">&times;</a>
                    </div></div>
                    @endif

        <div class="col-md-6">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{ trans('clients.addnew') }}</h3>
              </div>
        <div class="card-body">
        <form method="post" action="{{url('addClient')}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="tbl" value="{{encrypt('clients')}}">
            <div class="form-group">
                <input type="file"  accept="image/*" name="image" id="file"  onchange="loadFile(event)" style="display: none;"></p>
                <p><label for="file" style="cursor: pointer;">{{ trans('clients.image') }}</label></p>
                <p><img id="output" width="200" /></p>
            </div>
            <div class="form-group">
                <label>{{ trans('clients.link') }}</label>
                <input type="text" name="link" class="form-control">
            </div>
            <div class="form-group">
                <label>{{ trans('clients.status') }}</label>
                <select class="form-control" name="status">
                    <option>on</option>
                    <option>off</option>
                </select>
            </div>
        </div>
        <div class="card-footer">
        	<button class="btn btn-success">{{ trans('clients.addnew') }}</button>
                                    </div>
                                </form>
                        </div>
                    </div>
                    <div class="card-body table-responsive p-0">
            <div class="col-sm-6">
                <table class="table table-striped table-hover table-bordered">
                    <tr>
                        <th>{{ trans('clients.sn') }}</th>
                        <th>{{ trans('clients.image') }}</th>
                        <th>{{ trans('clients.link') }}</th>
                        <th>{{ trans('clients.status') }}</th>
                        <th>{{ trans('clients.action') }}</th>
                    </tr>
                    <tbody>
                    @if(!$data->isEmpty())
                    @foreach($data as $key=>$client)
                        <tr>
                            <td>{{++$key}}</td>
                            <td>@if($client->image)
                                    <img src="{{url('public/clients')}}/{{$client->image}}" width="100"></span>
                                @endif</td>
                            <td>@if($client->link)
                                    {{$client->link}}
                                    @endif
                            </td>
                            <td>{{$client->status}}</td>
                            <td><a href="{{url('editClient')}}/{{$client->cid}}" class="btn btn-sm btn-success"><i class="fa fa-edit"></i></a><a href="{{url('deleteClient')}}/{{$client->cid}}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a></td>
                        </tr>
                    @endforeach
                        @else
                    <tr><td colspan="5">{{ trans('breadcrumbs.nodata') }}</td></tr>
                        @endif
                    </tbody>
                </table>
            </div>
            </div>
            </div>
            </div>
</section>
    <script>
var loadFile = function(event) {
var image = document.getElementById('output');
image.src = URL.createObjectURL(event.target.files[0]);
};
</script>
@stop

