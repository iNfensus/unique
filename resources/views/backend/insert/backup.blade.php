@extends('backend.master')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="{{url('admin')}}" class="brand-link">
            <img src="{{url('public/backend/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                 style="opacity: .8">
            <span class="brand-text font-weight-light">AdminLTE 3</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="{{url('public/backend/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                    <a href="#" class="d-block">{{ Auth::user()->name }}</a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->
                    <li class="nav-item">
                        <a href="{{url('admin')}}" class="nav-link">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                {{ trans('adminleftmenu.dashboard') }}
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('setups')}}" class="nav-link">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                {{ trans('adminleftmenu.settings') }}
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="{{url('categories')}}" class="nav-link">
                            <i class="nav-icon fas fa-copy"></i>
                            <p>
                                {{ trans('adminleftmenu.categories') }}
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="" class="nav-link">
                            <i class="nav-icon fas fa-book"></i>
                            <p>
                                {{ trans('adminleftmenu.content') }}
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview" style="display: none;">
                            <li class="nav-item">
                                <a href="{{url('all-posts')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>{{ trans('adminleftmenu.allposts') }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('new-post')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>{{ trans('adminleftmenu.addnew') }}</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-book"></i>
                            <p>
                                {{ trans('adminleftmenu.service') }}
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{url('all-services')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>{{ trans('adminleftmenu.allservice') }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('new-service')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>{{ trans('adminleftmenu.addnewservice') }}</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-book"></i>
                            <p>
                                {{ trans('adminleftmenu.portfolio') }}
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{url('all-portfolio')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>{{ trans('adminleftmenu.allportfolios') }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('new-portfolio')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>{{ trans('adminleftmenu.addnewportfolio') }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('portcats')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>{{ trans('adminleftmenu.portcats') }}</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="{{url('clients')}}" class="nav-link">
                            <i class="nav-icon fas fa-book"></i>
                            <p>
                                {{ trans('adminleftmenu.clients') }}
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-book"></i>
                            <p>
                                {{ trans('adminleftmenu.team') }}
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{url('all-members')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>{{ trans('adminleftmenu.allmembers') }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('new-member')}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>{{ trans('adminleftmenu.newmember') }}</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="{{url('backup')}}" class="nav-link active">
                            <i class="nav-icon fas fa-folder"></i>
                            <p>
                                {{ trans('adminleftmenu.backups') }}
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{ trans('breadcrumbs.backups') }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('admin')}}">{{ trans('breadcrumbs.home') }}</a></li>
                        <li class="breadcrumb-item active">{{ trans('adminleftmenu.backups') }}</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">

                <div class="col-md-12">
                        <div class="card-header">
                            <h3 class="card-title">

                                <button id="create-new-backup-button" href="{{ url(config('backpack.base.route_prefix').'/backup/create') }}" class="btn btn-primary ladda-button mb-2" data-style="zoom-in">
                                <span class="ladda-label"><i class="fa fa-plus"></i>{{ trans('backup.create_a_new_backup') }}
                                </span>
                                </button>
                            </h3>

                        </div>
    <div class="card">
        <div class="card-body p-0">
            <table class="table table-hover pb-0 mb-0">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{{ trans('backup.location') }}</th>
                    <th>{{ trans('backup.date') }}</th>
                    <th class="text-right">{{ trans('backup.file_size') }}</th>
                    <th class="text-right">{{ trans('backup.actions') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($backups as $k => $b)
                    <tr>
                        <th scope="row">{{ $k+1 }}</th>
                        <td>{{ $b['disk'] }}</td>
                        <td>{{ \Carbon\Carbon::createFromTimeStamp($b['last_modified'])->formatLocalized('%d %B %Y, %H:%M') }}</td>
                        <td class="text-right">{{ round((int)$b['file_size']/1048576, 2).' MB' }}</td>
                        <td class="text-right">
                            @if ($b['download'])
                                <a class="btn btn-sm btn-link" href="{{ url(config('backpack.base.route_prefix').'/backup/download/') }}?disk={{ $b['disk'] }}&path={{ urlencode($b['file_path']) }}&file_name={{ urlencode($b['file_name']) }}"><i class="fa fa-download"></i> {{ trans('backup.download') }}</a>
                            @endif
                            <a class="btn btn-sm btn-link" data-button-type="delete" href="{{ url(config('backpack.base.route_prefix').'/backup/delete/'.$b['file_name']) }}?disk={{ $b['disk'] }}"><i class="fa fa-trash"></i> {{ trans('backup.delete') }}</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
            </div>
        </div>
            </div>
        </div>
    </section> </div>
        </div><!-- /.box-body -->

    <!-- /.box -->

    <script>
        jQuery(document).ready(function($) {
            // capture the Create new backup button
            $("#create-new-backup-button").click(function(e) {
                e.preventDefault();
                var create_backup_url = $(this).attr('href');
                // do the backup through ajax

                $.ajax({
                    url: create_backup_url,
                    type: 'PUT',
                    data: {
                        _token: '{!! csrf_token() !!}',
                    },

                    success: function(result) {
                        // Show an alert with the result
                        if (result.indexOf('failed') >= 0) {
                            new Noty({
                                text: "<strong>{{ trans('backup.create_warning_title') }}</strong><br>{{ trans('backup.create_warning_message') }}",
                                type: "warning",
                                animation: {
                                    open: 'animated bounceInRight', // Animate.css class names
                                    close: 'animated bounceOutRight' // Animate.css class names
                                }
                            }).show();
                            setInterval('location.reload()', 8000);        // Using .reload() method.
                        }
                        else
                        {
                            new Noty({
                                text: "<strong>{{ trans('backup.create_confirmation_title') }}</strong><br>{{ trans('backup.create_confirmation_message') }}",
                                type: "success",
                                animation: {
                                    open: 'animated bounceInRight', // Animate.css class names
                                    close: 'animated bounceOutRight' // Animate.css class names
                                }
                            }).show();
                            setInterval('location.reload()', 8000);        // Using .reload() method.
                        }
                    },
                    error: function(result) {
                        // Show an alert with the result
                        new Noty({
                            text: "<strong>{{ trans('backup.create_error_title') }}</strong><br>{{ trans('backup.create_error_message') }}",
                            type: "warning",
                            animation: {
                                open: 'animated bounceInRight', // Animate.css class names
                                close: 'animated bounceOutRight' // Animate.css class names
                            }
                        }).show();
                        setInterval('location.reload()', 8000);        // Using .reload() method.
                    }
                });
            });
            // capture the delete button
            $("[data-button-type=delete]").click(function(e) {
                e.preventDefault();
                var delete_button = $(this);
                var delete_url = $(this).attr('href');
                if (confirm("{{ trans('backup.delete_confirm') }}") == true) {
                    $.ajax({
                        url: delete_url,
                        type: 'DELETE',
                        data: {
                            _token: '{!! csrf_token() !!}',
                        },
                        success: function(result) {
                            // Show an alert with the result
                            new Noty({
                                text: "<strong>{{ trans('backup.delete_confirmation_title') }}</strong><br>{{ trans('backup.delete_confirmation_message') }}",
                                type: "success",
                                animation: {
                                    open: 'animated bounceInRight', // Animate.css class names
                                    close: 'animated bounceOutRight' // Animate.css class names
                                }
                            }).show();
                            // delete the row from the table
                            delete_button.parentsUntil('tr').parent().remove();
                        },
                        error: function(result) {
                            // Show an alert with the result
                            new Noty({
                                text: "<strong>{{ trans('backup.delete_error_title') }}</strong><br>{{ trans('backup.delete_error_message') }}",
                                type: "warning",
                                animation: {
                                    open: 'animated bounceInRight', // Animate.css class names
                                    close: 'animated bounceOutRight' // Animate.css class names
                                }
                            }).show();
                        }
                    });
                } else {
                    new Noty({
                        text: "<strong>{{ trans('backup.delete_cancel_title') }}</strong><br>{{ trans('backup.delete_cancel_message') }}",
                        type: "info",
                        animation: {
                            open: 'animated bounceInRight', // Animate.css class names
                            close: 'animated bounceOutRight' // Animate.css class names
                        }
                    }).show();
                }
            });
        });
    </script>
@endsection

