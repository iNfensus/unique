<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Dashboard</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="{{url('public/backend/css/fontawesome-free/css/all.css')}}">
    <link rel="stylesheet" href="{{url('public/backend/css/fontawesome-free/css/fontawesome.css')}}">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="{{url('public/backend/css/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
    <link rel="stylesheet" href="{{url('public/backend/css/adminlte.css')}}">
    <link rel="stylesheet" href="{{url('public/backend/css/noty.css')}}">
    <link rel="stylesheet" href="{{url('public/backend/css/animate.css')}}">
    <link rel="stylesheet" href="{{url('public/backend/css/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <link rel="stylesheet" href="{{url('public/backend/css/daterangepicker/daterangepicker.css')}}">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <script src="{{url('public/backend/js/jquery.min.js')}}"></script>
    <script src="{{url('public/backend/js/jquery.overlayScrollbars.min.js')}}"></script>
  <script src="{{url('public/backend/js/bootstrap.min.js')}}"></script>
  <script src="{{url('public/backend/js/adminlte.js')}}"></script>
    <script src="{{url('public/backend/js/noty.js')}}"></script>
    <script src="{{url('public/backend/js/moment.min.js')}}"></script>
    <script src="{{url('public/backend/js/daterangepicker.js')}}"></script>
    <script src="{{url('public/backend/css/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.js')}}"></script>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
        @foreach (language()->allowed() as $code => $name)
            <li class="nav-item d-none d-sm-inline-block">
                <a href="{{ language()->back($code) }}" class="nav-link">
                    <img src="{{ asset('vendor/akaunting/language/src/Resources/assets/img/flags/'. language()->country($code) .'.png') }}" alt="{{ $name }}" width="{{ config('language.flags.width') }}" />
                </a>
            </li>
        @endforeach
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{url('admin')}}" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="mailto:wrzeszczdamian@gmail.com" class="nav-link">Contact</a>
      </li>
      </ul>

      <!-- ADMIN FORM -->
      <ul class="navbar-nav ml-auto">
          <!-- Authentication Links -->
              <li class="nav-item dropdown">
                  <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                      {{ Auth::user()->name }} <span class="caret"></span>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="{{ url('/') }}" target="_blank">
                          {{ trans('admin.index') }}
                      </a>
                      <a class="dropdown-item" href="{{ route('logout') }}"
                         onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                          {{ trans('admin.logout') }}
                      </a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form>
                  </div>
              </li>

      </ul>
  </nav>
  <!-- /.navbar -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- /.content-header -->

    <!-- Main content -->
    @yield('content')
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.1
    </div>
  </footer>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
</body>
</html>
