<meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">
<section class="col-lg-7 connectedSortable">
    <!-- Bootstrap Boilerplate... -->
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">
                <i class="ion ion-clipboard mr-1"></i>
                {{ trans('todo.list') }}
            </h3>

            <div class="card-tools">
                <button type="button"
                        class="btn btn-sm"
                        data-card-widget="collapse"
                        data-toggle="tooltip"
                        title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-sm" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                </button>
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <ul class="todo-list" data-widget="todo-list">
                @if(!$taskdata->isEmpty())
                    @foreach ($taskdata as $task)
                        <li>
                            <!-- drag handle -->
                            <span class="handle">
                      <i class="fas fa-ellipsis-v"></i>
                      <i class="fas fa-ellipsis-v"></i>
                    </span>
                            <!-- checkbox -->
                            <div  class="icheck-primary d-inline ml-2">
                                <input type="checkbox" name="{{$task->name}}" id="{{$task->id}}" @if($task->todo == 1) checked @endif/>

                            </div>
                            <!-- todo text -->
                            <span class="text">{{ $task->name }}</span>
                            <!-- Emphasis label -->
                            <small class="badge badge-danger"><i class="far fa-clock"></i> {{ Carbon\Carbon::parse($task->created_at)->diffForHumans() }}</small>
                            <!-- General tools such as edit or delete-->
                            <div class="tools">
                                <form action="{{url('deleteTask')}}/{{$task->id}}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button class="btn btn-block bg-gradient-warning btn-sm">{{ trans('todo.delete') }}</button>
                                </form>
                            </div>
                        </li>
                    @endforeach
                @else
                    <tr><td class="table-text"><div>{{ trans('breadcrumbs.nodata') }}</div></td></tr>
                @endif
            </ul>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
            <form action="{{url('addTask')}}" method="POST" class="form-horizontal">
                {{ csrf_field() }}
                <input type="hidden" name="tbl" value="{{encrypt('tasks')}}">
                <!-- Task Name -->
                <div class="form-group">
                    <label for="task" class="col-sm-3 control-label">{{ trans('todo.task') }}</label>
                    <div class="row">
                        <div class="col-sm-9">
                            <input type="text" name="name" class="form-control">
                        </div>
                        <div class="col-sm-3">
                            <button type="submit" class="btn btn-info">
                                <i class="fa fa-plus"></i> {{ trans('todo.addtask') }}
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </section>
<script>
    $(document).ready(function(){
        $("input:checkbox").change(function() {
            var isChecked = $("input:checkbox").is(":checked") ? 1:0;
            var task_id = $(this).attr("id");
            var name = $(this).attr("name");
            $.ajax({
                type:'POST',
                url:'{{url('updatetaskstatus')}}',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                data: {
                    "isChecked": isChecked,
                    "task_id": task_id,
                    "name": name,
                },
                success:function(data){
                    console.log("sukces")
                },
                error:function(data){
                    console.log("fail")
                }
            });
        });
    });
</script>
