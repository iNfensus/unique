@extends('backend.master')
@section('content')

<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('admin')}}" class="brand-link">
      <img src="{{url('public/backend/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{url('public/backend/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{url('admin')}}" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            {{ trans('adminleftmenu.dashboard') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{url('setups')}}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            {{ trans('adminleftmenu.settings') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{url('categories')}}" class="nav-link active">
                        <i class="nav-icon fas fa-copy"></i>
                        <p>
                            {{ trans('adminleftmenu.categories') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="" class="nav-link">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            {{ trans('adminleftmenu.content') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview" style="display: none;">
                        <li class="nav-item">
                            <a href="{{url('all-posts')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.allposts') }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('new-post')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.addnew') }}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            {{ trans('adminleftmenu.service') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('all-services')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.allservice') }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('new-service')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.addnewservice') }}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            {{ trans('adminleftmenu.portfolio') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('all-portfolio')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.allportfolios') }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('new-portfolio')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.addnewportfolio') }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('portcats')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.portcats') }}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{url('clients')}}" class="nav-link">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            {{ trans('adminleftmenu.clients') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            {{ trans('adminleftmenu.team') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('all-members')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.allmembers') }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('new-member')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.newmember') }}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{url('backup')}}" class="nav-link">
                        <i class="nav-icon fas fa-folder"></i>
                        <p>
                            {{ trans('adminleftmenu.backups') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>


<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{ trans('breadcrumbs.editcategory') }}</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('admin')}}">{{ trans('breadcrumbs.home') }}</a></li>
                    <li class="breadcrumb-item active">{{ trans('adminleftmenu.editcategory') }}</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

     <section class="content">
        <div class="container-fluid">
            <div class="row">
                @if(Session::has('message'))
                <div class="col-sm-12">
                    <div class="alert alert-success alert-dismissable">
                        {{ session::get('message') }}
                        <a class="close" data-dismiss="alert">&times;</a>
                    </div></div>
                    @endif
        <div class="col-md-6">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{ trans('category.edit') }}</h3>
              </div>
        <div class="card-body">
        <form method="post" action="{{url('updateCategory')}}/{{$maindata->cid}}">
            {{ csrf_field() }}
            <input type="hidden" name="tbl" value="{{encrypt('categories')}}">
            <input type="hidden" name="cid" value="{{$maindata->cid}}">
        	<div class="form-group">
        		<label>{{ trans('category.name') }}</label>
        		<input type="text" name="title" value="{{$maindata->title}}"class="form-control" required>
        	</div>
            <div class="form-group">
                <label>{{ trans('category.status') }}</label>
                <select class="form-control" name="status">
                  <option>{{$maindata->status}}</option>
                  @if($maindata->status == 'off')
                    <option>on</option>
                    @else
                    <option>off</option>
                    @endif
                </select>
            </div>
        </div>
        <div class="card-footer">
        	<button class="btn btn-success">{{ trans('category.save') }}</button>
                        </div>
                     </form>

            </div>
        </div>
                    <div class="card-body table-responsive p-0">
            <div class="col-sm-6">
      <table class="table table-striped table-hover table-bordered">
          <tr>
              <th>{{ trans('category.sn') }}</th>
              <th>{{ trans('category.name') }}</th>
              <th>{{ trans('category.status') }}</th>
              <th>{{ trans('category.action') }}</th>
            </tr>
            <tbody>
            @if(!$data->isEmpty())
              @foreach($data as $key=>$category)
              <tr>
                <td>{{++$key}}</td>
                <td>{{str_limit($category->title, $limit = 10, $end = '...')}}</td>
                <td>{{$category->status}}</td>
                <td><a href="{{url('editCategory')}}/{{$category->cid}}" class="btn btn-sm btn-success"><i class="fa fa-edit"></i></a><a href="{{url('deleteCategory')}}/{{$category->cid}}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a></td>
              </tr>
              @endforeach
            @else
                <tr><td colspan="4">{{ trans('breadcrumbs.nodata') }}</td></tr>
            @endif
            </tbody>
          </table>
        </div>
    </div>
            </div>
        </div>
</section>
@stop
