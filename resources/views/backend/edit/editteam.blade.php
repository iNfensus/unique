@extends('backend.master')
@section('content')

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('admin')}}" class="brand-link">
      <img src="{{url('public/backend/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{url('public/backend/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{url('admin')}}" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            {{ trans('adminleftmenu.dashboard') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{url('setups')}}" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            {{ trans('adminleftmenu.settings') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{url('categories')}}" class="nav-link">
                        <i class="nav-icon fas fa-copy"></i>
                        <p>
                            {{ trans('adminleftmenu.categories') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            {{ trans('adminleftmenu.content') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('all-posts')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.allposts') }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('new-post')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.addnew') }}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link ">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            {{ trans('adminleftmenu.service') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('all-services')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.allservice') }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('new-service')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.addnewservice') }}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            {{ trans('adminleftmenu.portfolio') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('all-portfolio')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.allportfolios') }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('new-portfolio')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.addnewportfolio') }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('portcats')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.portcats') }}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{url('clients')}}" class="nav-link">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            {{ trans('adminleftmenu.clients') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview menu-open">
                    <a href="#" class="nav-link active">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            {{ trans('adminleftmenu.team') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{url('all-members')}}" class="nav-link active">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.allmembers') }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('new-member')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ trans('adminleftmenu.newmember') }}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{url('backup')}}" class="nav-link">
                        <i class="nav-icon fas fa-folder"></i>
                        <p>
                            {{ trans('adminleftmenu.backups') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <div class="content-header">
      <div class="container-fluid">
          <div class="row mb-2">
              <div class="col-sm-6">
                  <h1 class="m-0 text-dark">{{ trans('breadcrumbs.editeam') }}</h1>
              </div><!-- /.col -->
              <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                      <li class="breadcrumb-item"><a href="{{url('admin')}}">{{ trans('breadcrumbs.home') }}</a></li>
                      <li class="breadcrumb-item active">{{ trans('adminleftmenu.editeam') }}</li>
                  </ol>
              </div><!-- /.col -->
          </div><!-- /.row -->
      </div><!-- /.container-fluid -->
  </div>

     <section class="content">
        <div class="container-fluid">
            <div class="row">
                @if(Session::has('message'))
                <div class="col-sm-12">
                    <div class="alert alert-success alert-dismissable">
                        {{ session::get('message') }}
                        <a class="close" data-dismiss="alert">&times;</a>
                    </div></div>
                    @endif

        <div class="col-md-6">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{ trans('team.edit') }}</h3>
              </div>
        <div class="card-body">
        <form method="post" action="{{url('updateMember')}}/{{$data->tid}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="tbl" value="{{encrypt('teams')}}">
            <input type="hidden" name="tid" value="{{$data->tid}}">
        	<div class="form-group">
        		<label>{{ trans('team.name') }}</label>
        		<input type="text" name="name" class="form-control" value="{{$data->name}}" required>
        	</div>
            <div class="form-group">
                <label>{{ trans('team.designation') }}</label>
                <input type="text" name="designation" class="form-control" value="{{$data->designation}}">
            </div>
            <div class="form-group">
                <label>{{ trans('team.intro') }}</label>
                <textarea name="intro" class="form-control" rows="12">{{$data->intro}}</textarea>
            </div>
            <div class="form-group">
                <input type="file"  accept="image/*" name="image" id="file"  onchange="loadFile(event)" style="display: none;"></p>
                <p><label for="file" style="cursor: pointer;">{{ trans('team.image') }}</label></p>
                @if($data->image)
                    <p><img id="output" src="{{url('public/teams')}}/{{$data->image}}"/></p>
                @else
                    <p><img id="output" width="200" /></p>
                @endif
            </div>
            <div class="form-group">
                <label>{{ trans('team.social') }}</label>
            </div>
            <div id="socialGroup">
                @foreach($socials as $social)
                <div class="form-group socialField">
                    <input type="text" name="social[]" class="form-control" value="{{$social}}">
                    <a href="#" class="btn btn-warning addField"><i class="fa fa-plus"></i></a>
                    <a href="#" class="btn btn-warning removeField"><i class="fa fa-minus"></i></a>
                </div>
                    @endforeach
            </div>
            <div class="alert alert-danger" id="socialError">
                <strong>{{ trans('team.sorry') }}</strong> {{ trans('team.warning') }}
            </div>
            <div class="form-group">
                <label>{{ trans('team.status') }}</label>
                <select class="form-control" name="status">
                    <option>{{$data->status}}</option>
                    @if($data->status == 'off')
                        <option>on</option>
                    @else
                        <option>off</option>
                    @endif
                </select>
            </div>
            </div>
        <div class="card-footer">
        	<button class="btn btn-success">{{ trans('team.save') }}</button>
                        </div>
                     </form>
                </div>
            </div>
        </div>
    </div>
</section>
  <script>
      var loadFile = function(event) {
          var image = document.getElementById('output');
          image.src = URL.createObjectURL(event.target.files[0]);
      };
  </script>
  <script>
      fieldCounter = 1;
      $('.addField').click(function(){
          fieldCounter++;
          if(fieldCounter <= 4) {
              var newField = $(document.createElement('div')).attr('class','form-group');
              newField.after().html('<input type="text" name="social[]" class="form-control"></div>');
              newField.appendTo('#socialGroup');
          }else{
              $('#socialError').show();
          }
      })
      $('.removeField').click(function() {
        $(this).parent('div').attr('class','form-group').empty();
      }
      )
  </script>
  <style>
      .socialField{
          position: relative;
      }
      .addField{
          position: absolute;
          top: 0;
          right: 0;
      }
      #socialError{
          display: none;
      }
  </style>
  <script src="{{url('resources/views/backend/ckeditor/ckeditor.js')}}"></script>
  <script>CKEDITOR.replace('intro', {});</script>
@stop

