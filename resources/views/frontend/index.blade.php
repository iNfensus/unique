<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, maximum-scale=1">
@if(isset($setups->keywords))
<meta name="keywords" content="{{$setups->keywords}}">
@else
@endif
@if(isset($setups->description))
<meta name="description" content="{{$setups->description}}">
@else
@endif
    <meta property="og:title" content="{{$setups->meta_title}}">
    <meta property="og:description" content="{{$setups->description}}">
    <meta property="og:image" content="{{url('public/contents')}}/{{$setups->image}}">
    <meta property="og:url" content="{{url('/')}}">
    <meta property="og:site_name" content="{{$setups->meta_title}}">
    <meta property="og:see_also" content="{{url('/')}}">
<link rel="canonical" href="{{url('/')}}">
<title>{{$setups->meta_title}}</title>
<link rel="icon" href="{{url('public/setups')}}/{{$setups->image}}" type="image/png">
<link href="{{url('public/frontend/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{url('public/frontend/css/style.css')}}" rel="stylesheet" type="text/css">
<link href="{{url('public/frontend/css/font-awesome.css')}}" rel="stylesheet" type="text/css">
<link href="{{url('public/frontend/css/animate.css')}}" rel="stylesheet" type="text/css">

<!--[if lt IE 9]>
<script src="{{url('public/frontend/js/respond-1.1.0.min.js')}}"></script>
<script src="{{url('public/frontend/js/html5shiv.js')}}"></script>
<script src="{{url('public/frontend/js/html5element.js')}}"></script>
<![endif]-->
    @if(isset($setups->gacode))
        <!-- Google Analytics -->
            <script>
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                ga('create', '{{$setups->gacode}}', 'auto');
                ga('send', 'pageview');
            </script>
            <!-- End Google Analytics -->
    @else
    @endif
</head>
<body>

<!--Header_section-->
<header id="header_wrapper">
  <div class="container">
    <div class="header_box">
      <div class="logo"><a href="#"><img src="{{url('public/setups')}}/{{$setups->image}}" alt="logo"></a></div>
	  <nav class="navbar navbar-inverse" role="navigation">
      <div class="navbar-header">
        <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
	    <div id="main-nav" class="collapse navbar-collapse navStyle">
			<ul class="nav navbar-nav" id="mainNav">
        @foreach($cats as $cat)
			  <li class="text-uppercase"><a href="#{{$cat->slug}}" class="scroll-link">{{$cat->title}}</a></li>
        @endforeach
        <li class="text-uppercase"><a href="#contact" class="scroll-link">contact us</a></li>
			</ul>
      </div>
	 </nav>
    </div>
  </div>
</header>
<!--Header_section-->

@if(isset($home->status))
    <!--Hero_Section-->
    <section id="{{$home->slug}}" class="top_cont_outer">
        <div class="hero_wrapper">
            <div class="container">
                <div class="hero_section">
                    <div class="row">
                        <div class="col-lg-5 col-sm-7">
                            <div class="top_left_cont zoomIn wow animated">
                                <h1 class="text-uppercase">{{$home->title}}</h1>
                                <p>{!! $home->description !!}</p>
                                <a href="#{{$home->link}}" class="read_more2">Read more</a> </div>
                        </div>
                        <div class="col-lg-7 col-sm-5">
                            <img src="{{url('public/contents')}}/{{$home->image}}" alt="Main slider image" class="zoomIn wow animated" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @else
    <!--Empty Hero_Section if no data or data status is off-->
@endif
@if(isset($about->status))
    <!--Aboutus-->
    <section id="{{$about->slug}}"><!--Aboutus-->
        <div class="inner_wrapper">
            <div class="container">
                <h2 class="text-uppercase">{{$about->title}}</h2>
                <div class="inner_section">
                    <div class="row">
                        <div class=" col-lg-4 col-md-4 col-sm-4 col-xs-12 pull-right"><img src="{{url('public/contents')}}/{{$about->image}}" class="img-circle delay-03s animated wow zoomIn" alt="About image"></div>
                        <div class=" col-lg-7 col-md-7 col-sm-7 col-xs-12 pull-left">
                            <div class=" delay-01s animated fadeInDown wow animated">
                                {!! $about->description !!}
                            </div>
                            <div class="work_bottom"><a href="#contact" class="contact_btn">Contact Us</a> </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    @else
    <!--Empty About Us Section  if no data or data status is off-->
    @endif
@if(isset($services->status))
    <!--Servies Section -->
    <section id="{{$services->slug}}">
        <div class="container">
            <h2>Services</h2>
            <div class="service_wrapper">
                <div class="row">
                    @foreach($services as $service)
                        <div class="col-lg-4">
                            <div class="service_block">
                                <div class="service_icon delay-03s animated wow  zoomIn"> <span><i class="fa fa-{{$service->icon}}"></i></span> </div>
                                <h3 class="animated fadeInUp wow">{{$service->title}}</h3>
                                <p class="animated fadeInDown wow">{!!$service->description!!}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    @else
    <!--Empty Services Section  if no data or data status is off-->
    @endif

@if(isset($portcats->status) && ($portfolio->status))
<!-- Portfolio -->
<section id="{{$portfolio->slug}}" class="content">
  <!-- Container -->
  <div class="container portfolio_title">
    <!-- Title -->
    <div class="section-title">
      <h2>Portfolio</h2>
    </div>
    <!--/Title -->
  </div>
  <!-- Container -->
  <div class="portfolio-top"></div>
        <!--Empty Portfolio Section  if no data in portfolio and portfolio categories or data status is off-->
  <!-- Portfolio Filters -->
  <div class="portfolio">
    <div id="filters" class="sixteen columns">
      <ul class="clearfix">
        <li><a id="all" href="#" data-filter="*" class="active">
          <h5>All</h5>
          </a></li>
          @foreach($portcats as $cats)
        <li><a class="" href="#" data-filter=".{{$cats->slug}}">
          <h5>{{$cats->title}}</h5>
          </a></li>
          @endforeach
      </ul>
    </div>
      @else
          <!--Empty Portfolio Categories Section  if no data or data status is off-->
          @endif
    <!--/Portfolio Filters -->
    <!-- Portfolio Wrapper -->
      @if(isset($portfolio->status))
    <div class="isotope fadeInLeft animated wow" style="position: relative; overflow: hidden; height: 480px;" id="portfolio_wrapper">
@foreach($portfolio as $port)
      <!-- Portfolio Item -->
      <div style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1); width: 337px; opacity: 1;" class="portfolio-item one-four {{$port->class}} isotope-item">
        <div class="portfolio_img"> <img src="{{url('public/portfolios')}}/{{$port->image}}"  alt="Portfolio 1"> </div>
        <div class="item_overlay">
          <div class="item_info">
            <h4 class="project_name">{{$port->title}}</h4>
          </div>
        </div>
        </div>
      <!--/Portfolio Item -->
@endforeach
    </div>
    <!--/Portfolio Wrapper -->
@else
      <!--Empty Porftolio Section if no data or data status is off-->
      @endif
  </div>
  <!--/Portfolio Filters -->

  <div class="portfolio_btm"></div>


  <div id="project_container">
    <div class="clear"></div>
    <div id="project_data"></div>
  </div>


</section>
<!--/Portfolio -->
@if(isset($clients->status))
<section class="page_section" id="{{$clients->slug}}"><!--page_section-->
  <h2>Clients</h2>
<!--page_section-->
    <!--client_logos-->
<div class="client_logos">
  <div class="container">
    <ul class="fadeInRight animated wow">
        @foreach($clients->take(4) as $client)
      <li><a href="{{$client->link}}"><img src="{{url('public/clients')}}/{{$client->image}}" alt="clients image"></a></li>
       @endforeach
    </ul>
  </div>
</div>
</section>
<!--client_logos-->
@else
    <!--Empty Clients Section if no data or data status is off-->
    @endif
@if(isset($teams->status))
<!--Team-->
<section class="page_section team" id="{{$teams->slug}}"><!--main-section team-start-->
  <div class="container">
    <h2>Team</h2>
    <h6>Lorem ipsum dolor sit amet, consectetur adipiscing.</h6>
    <div class="team_section clearfix">
        @foreach($teams as $team)
      <div class="team_area">
        <div class="team_box wow fadeInDown delay-03s">
          <div class="team_box_shadow"><a href="javascript:void(0)"></a></div>
          <img src="{{url('public/teams')}}/{{$team->image}}" alt="{{$team->name}}">
          <ul>
              @foreach($team->teamurl as $key=>$url)
            <li><a href="{{$url}}" class="fa fa-{{$team->font[$key]}}"></a></li>
              @endforeach
          </ul>
        </div>
        <h3 class="wow fadeInDown delay-03s">{{$team->name}}</h3>
        <span class="wow fadeInDown delay-03s">{{$team->designation}}</span>
        <p class="wow fadeInDown delay-03s">{!!$team->intro!!}</p>
      </div>
        @endforeach
    </div>
  </div>
</section>
<!--/Team-->
@else
    <!--Empty Teams Section if no data or data status is off-->
    @endif
<!--Footer-->
<footer class="footer_wrapper" id="contact">
    <div class="container">
        <section class="page_section contact" id="contact">
            <div class="contact_section">
                <h2>Contact Us</h2>
                <div class="row">
                    <div class="col-lg-4">
                    </div>
                    <div class="col-lg-4">
                    </div>
                    <div class="col-lg-4">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 wow fadeInLeft">
                    <div class="contact_info">
                        <div class="detail">
                            <h4>{{$setups->meta_title}}</h4>
                            <p>{{$setups->address}}</p>
                        </div>
                        <div class="detail">
                            <h4>call us</h4>
                            <p>{{$setups->contact}}</p>
                        </div>
                        <div class="detail">
                            <h4>Email us</h4>
                            <p>{{$setups->email}}</p>
                        </div>
                    </div>
                    <ul class="social_links">
                        @foreach($socials as $key=>$social)
                            <li class="{{$fa[$key]}} animated bounceIn wow delay-02s"><a href="{{$social}}"><i class="fa fa-{{$fa[$key]}}"></i></a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-lg-8 wow fadeInLeft delay-06s">
                    <div class="form">
                        <form>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="control-group">
                                <div class="controls">
                                    <input type="text" class="form-control input-text" minlength="3"
                                           placeholder="Full Name" name="name" id="name" required data-validation-required-message="Please enter your name">
                                </div></div>
                            <div class="control-group">
                                <div class="controls">
                                    <input type="email" class="form-control input-text" placeholder="Email"
                                           name="email" id="email" required
                                           data-validation-required-message="Please enter your email">
                                </div></div>
                            <div class="control-group">
                                <div class="controls">
                                    <textarea rows="10" cols="100" class="form-control input-text"
                                          placeholder="Message" name="msg" id="msg" required
                                              data-validation-required-message="Please enter your message" minlength="5"
                                              data-validation-minlength-message="Min 5 characters" style="resize:none"></textarea>
                                </div></div>
                            <div id="success"> </div> <!-- For success/fail messages -->
                            <button type="submit" class="btn btn-primary input-btn pull-right">Send</button><br />
                        </form>
                    </div>
    </div>
        </section>
    <div class="container">
        <div class="footer_bottom"><span>Copyright © {{ date('Y') }}. <a href="https://webthemez.com/free-bootstrap-templates/" target="_blank">Free Bootstrap Theme</a> by WebThemez </span> </div>
    </div>
</footer>
<script type="text/javascript" src="{{url('public/frontend/js/jquery-1.11.0.min.js')}}"></script>
<script type="text/javascript" src="{{url('public/frontend/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{url('public/frontend/js/jquery-scrolltofixed.js')}}"></script>
<script type="text/javascript" src="{{url('public/frontend/js/jquery.nav.js')}}"></script>
<script type="text/javascript" src="{{url('public/frontend/js/jquery.easing.1.3.js')}}"></script>
<script type="text/javascript" src="{{url('public/frontend/js/jquery.isotope.js')}}"></script>
<script type="text/javascript" src="{{url('public/frontend/js/wow.js')}}"></script>
<script src="{{url('public/frontend/contact/jqBootstrapValidation.js')}}"></script>
<script>
    /*
  Jquery Validation using jqBootstrapValidation
   example is taken from jqBootstrapValidation docs
  */
    $(function() {

        $("input,textarea").jqBootstrapValidation(
            {
                preventSubmit: true,
                submitError: function($form, event, errors) {
                    // something to have when submit produces an error ?
                    // Not decided if I need it yet
                },
                submitSuccess: function($form, event) {
                    event.preventDefault(); // prevent default submit behaviour
                    // get values from FORM
                    var name = $('input[name=name]').val();
                    var email = $('input[name=email]').val();
                    var msg = $('textarea[name=msg]').val();

                    var firstName = name; // For Success/Failure Message
                    // Check for white space in name for Success/Fail message
                    if (firstName.indexOf(' ') >= 0) {
                        firstName = name.split(' ').slice(0, -1).join(' ');
                    }
                    $.ajax({
                        url: "{{url('contact')}}",
                        type: "POST",
                        data: {'name': $('input[name=name]').val(),
                            'email': $('input[name=email]').val(),
                            'msg': $('textarea[name=msg]').val(),
                            '_token': $('input[name=_token]').val()
                },
                        cache: false,
                        success: function() {
                            // Success message
                            $('#success').html("<div class='alert alert-success'>");
                            $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                .append( "</button>");
                            $('#success > .alert-success')
                                .append("<strong>Your message has been sent. </strong>");
                            $('#success > .alert-success')
                                .append('</div>');

                            //clear all fields
                            $('#contactForm').trigger("reset");
                        },
                        error: function() {
                            // Fail message
                            $('#success').html("<div class='alert alert-danger'>");
                            $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                .append( "</button>");
                            $('#success > .alert-danger').append("<strong>Sorry "+firstName+" it seems that my mail server is not responding...</strong> Could you please email me directly to <a href='mailto:me@example.com?Subject=Message_Me from myprogrammingblog.com'>me@example.com</a> ? Sorry for the inconvenience!");
                            $('#success > .alert-danger').append('</div>');
                            //clear all fields
                            $('#contactForm').trigger("reset");
                        },
                    })
                },
                filter: function() {
                    return $(this).is(":visible");
                },
            });

        $("a[data-toggle=\"tab\"]").click(function(e) {
            e.preventDefault();
            $(this).tab("show");
        });
    });


    /*When clicking on Full hide fail/success boxes */
    $('#name').focus(function() {
        $('#success').html('');
    });
</script>
<script type="text/javascript" src="{{url('public/frontend/js/custom.js')}}"></script>
</body>
</html>
