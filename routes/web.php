<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'frontController@index');

Route::group(['middleware' => 'language'], function () {
    Auth::routes();
    Route::get('admin', 'adminController@admin');
    Route::post('addTask', 'crudController@insertData');
    Route::delete('deleteTask/{id}','adminController@deleteTask');
    Route::post('updatetaskstatus', 'adminController@updateTask');
    Route::get('setups', 'adminController@setups');
    Route::get('categories', 'adminController@categories');
    Route::post('addSettings', 'crudController@insertData');
    Route::post('updateSettings', 'crudController@updateData');
    Route::post('addCategory', 'crudController@insertData');
    Route::get('deleteCategory/{id}', 'adminController@deleteCategory');
    Route::get('editCategory/{id}', 'adminController@editCategory');
    Route::post('updateCategory/{id}', 'crudController@updateData');
    Route::get('new-post', 'adminController@newPost');
    Route::post('addPost', 'crudController@insertData');
    Route::get('all-posts', 'adminController@allPosts');
    Route::get('editPost/{id}', 'adminController@editPost');
    Route::post('updatePost/{id}', 'crudController@updateData');
    Route::get('deletePost/{id}', 'adminController@deletePost');
    Route::get('backup', 'BackupController@index');
    Route::put('backup/create', 'BackupController@create');
    Route::get('backup/download/{file_name?}', 'BackupController@download');
    Route::delete('backup/delete/{file_name?}', 'BackupController@delete')->where('file_name', '(.*)');
    Route::get('new-service', 'adminController@newService');
    Route::post('addService', 'crudController@insertData');
    Route::get('all-services', 'adminController@allServices');
    Route::get('editService/{id}', 'adminController@editService');
    Route::post('updateService/{id}', 'crudController@updateData');
    Route::get('deleteService/{id}', 'adminController@deleteService');
    Route::get('portcats', 'adminController@portcats');
    Route::post('addPc', 'crudController@insertData');
    Route::get('deletepc/{id}', 'adminController@deletepc');
    Route::get('new-portfolio', 'adminController@portfolio');
    Route::post('addPortfolio', 'crudController@insertData');
    Route::get('editpc/{id}', 'adminController@editpc');
    Route::post('updatePortfolioCategory/{id}', 'crudController@updateData');
    Route::get('all-portfolio', 'adminController@allPortfolio');
    Route::get('deletePortfolio/{id}', 'adminController@deletePortfolio');
    Route::get('editPortfolio/{id}', 'adminController@editPortfolio');
    Route::post('updatePortfolio/{id}', 'crudController@updateData');
    Route::get('clients', 'adminController@clients');
    Route::post('addClient', 'crudController@insertData');
    Route::get('deleteClient/{id}', 'adminController@deleteClient');
    Route::get('editClient/{id}', 'adminController@editClient');
    Route::post('updateClient/{id}', 'crudController@updateData');
    Route::get('new-member', 'adminController@newMember');
    Route::post('addMember', 'crudController@insertData');
    Route::get('all-members', 'adminController@allMembers');
    Route::get('deleteMember/{id}', 'adminController@deleteMember');
    Route::get('editMember/{id}', 'adminController@editMember');
    Route::post('updateMember/{id}', 'crudController@updateData');
    Route::post('contact', 'frontController@postContact');
});
