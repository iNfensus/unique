<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Auth\Middleware\Authenticate;
use session;
use DB;
use Carbon\Carbon;

class adminController extends Controller
{
    public function __construct(){
         $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function admin()
    {
        $cats = DB::table('categories')->where('status', 'on')->get();
        $services = DB::table('services')->where('status', 'on')->get();
        $portfolio = DB::table('portfolios')->where('status', 'on')->get();
        $teams = DB::table('teams')->where('status', 'on')->get();
        $taskdata = DB::table('tasks')->get();

        return view('backend.index', [
            'cats' => $cats,
            'services' => $services,
            'portfolio' => $portfolio,
            'teams' => $teams,
            'taskdata'=>$taskdata,
        ]);
    }
    public function backups(){
        return view ('backend.insert.backup');
    }

    public function setups(){
    	$data = DB::table('setups')->first();
    	if(!empty($data)){
    		$socials = explode(',',$data->social);
    	}else{
    		$socials = [];
    	}
    	return view ('backend.insert.setup',['data'=>$data,'socials'=>$socials]);
    }

    public function categories(){
        $data = DB::table('categories')->get();
        return view ('backend.insert.category', ['data'=>$data]);
    }

    public function deleteCategory($id){
            $data = DB::table('categories')->where('cid',$id)->delete();
            return redirect()->back()->with('message', 'Data deleted succesfuly');
    }

    public function editCategory($id){
            $data = DB::table('categories')->get();
            $maindata = DB::table('categories')->where('cid',$id)->first();
            if($maindata){
                return view ('backend.edit.category', ['data'=>$data,'maindata'=>$maindata]);
            }else{
                return redirect ('categories');
        }
    }
    public function newPost(){
        $cats = DB::table('categories')->where('status', 'on')->get();
        return view ('backend.insert.newpost', ['cats'=>$cats]);

    }

    public function allPosts(){
        $data = DB::table('contents')->get();
        return view ('backend.display.post', ['data'=>$data]);
    }
    public function editPost($id){
        $cats = DB::table('categories')->where('status', 'on')->get();
        $data = DB::table('contents')->where('conid', $id)->first();
        return view ('backend.edit.editpost', ['data'=>$data, 'cats'=>$cats]);
    }

    public function deletePost($id){
        $data = DB::table('contents')->where('conid',$id)->delete();
        return redirect()->back()->with('message', 'Data deleted succesfuly');
    }

    public function newService(){
        return view ('backend.insert.service');

    }

    public function allServices(){
        $data = DB::table('services')->get();
        return view ('backend.display.service', ['data'=>$data]);
    }

    public function editService($id){
        $data = DB::table('services')->where('sid', $id)->first();
        return view ('backend.edit.editservice', ['data'=>$data]);
    }

    public function deleteService($id){
        $data = DB::table('services')->where('sid',$id)->delete();
        return redirect()->back()->with('message', 'Data deleted succesfuly');
    }

    public function portcats(){
        $data = DB::table('portcats')->get();
        return view ('backend.insert.portfolio-category', ['data'=>$data]);
    }

    public function portfolio(){
        $cats = DB::table('portcats')->where('status', 'on')->get();
        return view ('backend.insert.portfolio', ['cats'=>$cats]);
    }

    public function deletepc($id){
        $data = DB::table('portcats')->where('pcid',$id)->delete();
        return redirect()->back()->with('message', 'Data deleted succesfuly');
    }

    public function editpc($id){
        $data = DB::table('portcats')->get();
        $maindata = DB::table('portcats')->where('pcid',$id)->first();
        if($maindata){
            return view ('backend.edit.editportfolio-category', ['data'=>$data,'maindata'=>$maindata]);
        }else{
            return redirect ('portcats');
        }
    }

    public function allPortfolio(){
        $data = DB::table('portfolios')->get();
        return view ('backend.display.portfolio', ['data'=>$data]);
    }

    public function deletePortfolio($id){
        $data = DB::table('portfolios')->where('pid',$id)->delete();
        return redirect()->back()->with('message', 'Data deleted succesfuly');
    }

    public function editPortfolio($id){
        $data = DB::table('portfolios')->where('pid', $id)->first();
        $cats = DB::table('portcats')->where('status', 'on')->get();
        return view ('backend.edit.editportfolio', ['data'=>$data, 'cats'=>$cats]);
    }

    public function clients(){
        $data = DB::table('clients')->get();
        return view ('backend.insert.client', ['data'=>$data]);
    }
    public function deleteClient($id){
        $data = DB::table('clients')->where('cid',$id)->delete();
        return redirect()->back()->with('message', 'Data deleted succesfuly');
    }

    public function editClient($id){
        $data = DB::table('clients')->get();
        $maindata = DB::table('clients')->where('cid',$id)->first();
        if($maindata){
            return view ('backend.edit.editclient', ['data'=>$data,'maindata'=>$maindata]);
        }else{
            return redirect ('clients');
        }
    }
    public function newMember(){
        return view ('backend.insert.team');
    }
    public function allMembers(){
        $data = DB::table('teams')->get();
        return view ('backend.display.team', ['data'=>$data]);
    }
    public function deleteMember($id){
        $data = DB::table('teams')->where('tid',$id)->delete();
        return redirect()->back()->with('message', 'Data deleted succesfuly');
    }

    public function editMember($id){
        $data = DB::table('teams')->where('tid',$id)->first();
        if(!empty($data)){
            $socials = explode(',',$data->social);
        }else{
            $socials = [];
        }
        return view ('backend.edit.editteam',['data'=>$data,'socials'=>$socials]);
    }

    public function deleteTask($id){
        $taskdata = DB::table('tasks')->where('id',$id)->delete();
        return redirect()->back()->with('message', 'Data deleted succesfuly');
    }
    public function updateTask(Request $request){
        $task = Task::find($request->task_id);

        if($task->todo == 1){
            $task->todo = 0;
        } else {
            $task->todo = 1;
        }

        return response()->json([
            'data' => [
                'success' => $task->save(),
            ]
        ]);
    }
}
