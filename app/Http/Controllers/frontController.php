<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class frontController extends Controller
{
    public function __construct(){

    }

    public function index()
    {
        $setups = DB::table('setups')->first();
        if (!empty($setups)) {
            $socials = explode(',', $setups->social);
            $icons = explode(',', $setups->social);
            foreach ($icons as $icon) {
                $icon = explode('.', $icon);
                $icon = $icon[1];
                $fa[] = $icon;
            }
        } else {
            $socials = [];
        }

        $cats = DB::table('categories')->where('status', 'on')->get();
        $home = DB::table('contents')->where('category', 'home')->first();
        if (!empty($home)) {
            $home->status = DB::table('contents')->where([['category', 'home'], ['status', 'on']])->first();
            $home->slug = DB::table('categories')->where('title', 'home')->value('slug');
        } else {

        }
        $about = DB::table('contents')->where('category', 'about us')->first();
        if (!empty($about)) {
            $about->status = DB::table('contents')->where([['category', 'about us'], ['status', 'on']])->first();
            $about->slug = DB::table('categories')->where('title', 'about us')->value('slug');
        } else {

        }
    	$services = DB::table('services')->where('status', 'on')->get();
        if (!empty($services)) {
            $services->status = DB::table('services')->where('status', 'on')->first();
            $services->slug = DB::table('categories')->where('title', 'services')->value('slug');
        } else {

        }
        $portfolio = DB::table('portfolios')->where('status', 'on')->get();
        if (!empty($portfolio)) {
            $portfolio->status = DB::table('portfolios')->where('status', 'on')->first();
            foreach ($portfolio as $port) {
                $port->class = DB::table('portcats')->where('title', $port->category)->value('slug');
            }
            $portfolio->slug = DB::table('categories')->where('title', 'portfolio')->value('slug');
        } else {

        }
        $portcats = DB::table('portcats')->where('status', 'on')->get();
        if (!empty($portcats)) {
            $portcats->status = DB::table('portcats')->where('status', 'on')->first();
        } else {

        }
        $clients = DB::table('clients')->where('status', 'on')->get();
        if (!empty($clients)) {
            $clients->status = DB::table('clients')->where('status', 'on')->first();
            $clients->slug = DB::table('categories')->where('title', 'clients')->value('slug');
        } else {

        }
        $teams = DB::table('teams')->where('status', 'on')->get();
        if (!empty($clients)) {
            foreach ($teams as $team){
                $team->teamurl = explode(',',$team->social);
                foreach ($team->teamurl as $url){
                    $icon = explode('.',$url);
                    $icon = $icon[1];
                    $team->font[] = $icon;
                }
            }
            $teams->slug = DB::table('categories')->where('title', 'team')->value('slug');
            $teams->status = DB::table('teams')->where('status', 'on')->first();
        } else {

        }
    	return view  ('frontend.index',[
    		'setups' => $setups,
    		'socials' => $socials,
    		'fa' => $fa,
            'cats' => $cats,
            'home' => $home,
            'about' => $about,
            'services' => $services,
            'portfolio' => $portfolio,
            'portcats' => $portcats,
            'clients' => $clients,
            'teams' => $teams,
    	]);
    }
    public function postContact(Request $request){

        $this->validate($request, [
            'name' => 'min:3',
            'email' => 'required|email',
            'msg' => 'min:5'
        ]);

        $data = array(
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'msg' => $request->get('msg')
        );

        $email = DB::table('setups')->value('email');

        Mail::send('frontend.contact', $data, function($message) use($data, $email) {
            $message->from($data['email']);
            $message->to($email);
            $message->subject('Message from website contact form');
        });

    }
}
