<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class content extends Model
{
    protected $table = 'contents';
    protected $primaryKey = 'conid';
}
