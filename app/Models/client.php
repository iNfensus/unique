<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class client extends Model
{
    protected $table = 'clients';
    protected $primaryKey = 'cid';
}
