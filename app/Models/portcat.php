<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class portcat extends Model
{
    protected $table = 'portcats';
    protected $primaryKey = 'pcid';
}
