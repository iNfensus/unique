<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>
<p align="center"><img src="http://infensus.site/unique/public/setups/200129111321logo.png" width="100"></p>
<p align="center"><img src="https://cdn.worldvectorlogo.com/logos/adminlte.svg" width="100"></p>

# About my first project

My learning of laravel ;-)
This is single (one page) website with cms admin panel.
- [LIVE DEMO FRONT](http://www.infensus.site/unique)
- [LIVE DEMO ADMIN](http://www.infensus.site/unique/admin)
Login: admin@admin.com
Password: admin

#FRONT END 
(based on Unique Free Responsive Website Template)
- [THEME DEMO](https://www.themezy.com/free-website-templates/97-unique-free-responsive-website-template)
- [THEME DOWNLOAD](https://www.themezy.com/download/97?download_auth_hash=ae0260bf&pro=false) 

Main features of FRONT END
- Clean and minimalist layout
- Responsive (mobile friendly),
- Technology (HTML5 / CSS3 / Bootstrap / JavaScript (jquery)),
- 7 sections (Home, About us, Services, Portfolio, Clients, Team, Contact us),
- All data can be added/edited/deleted from admin panel,
- Working Contact form,
- SEO ready (meta data, og, google analytics tracking).
- Demo data included!

#BACKEND
(based on AdminLTE 3)
- [ADMINLTE DEMO](https://adminlte.io/themes/dev/AdminLTE/index.html)
- [ADMINLTE DOWNLOAD](https://github.com/ColorlibHQ/AdminLTE/releases)

Main features of BACK END
- Responsive
- Technology (HTML5 / CSS / Bootstrap / JS (jquery/ajax) / MySQL),
- Auth login/logout/register 
- Dashboard page (Working TODO List, Calendar),
- Settings page (to set up website: logo, title, phone number, address, email, social links, seo settings),
- Backup Manager (based on Spatie Backup),
- All data (page content) can be added/edited or deleted,
- Multilanguage - (English/Polish lang files included),
- Lang switcher - (you can simply change your backend lang).
= Demo data included!

# Requirements
- Laravel 6
- PHP 7
- MySQL Database

# How to Use
- Clone this repository, use git clone command: "git clone https://gitlab.com/iNfensus/unique.git"
or download ZIP https://gitlab.com/iNfensus/unique/-/archive/master/unique-master.zip
- Run command "composer install"
- Copy .env.example and rename it to ".env", set your app name, app url, env (type: local or production), provide your database details. 
- Run key generate with command "php artisan key:generate"
- Run migration with command "php artisan migrate"
- Run database (demo data) seeder with command "php artisan db:seed"
- Use "php artisan serve" or put it under real web server like apache
- Open the web with your favorite browser
- Go to your website url with /admin to open the admin panel, login: admin@admin.com password: admin
- Good luck & Have fun.

# Use Contact form
- Provide your e-mail address inside admin (settings)
- Open your .env file and provide <br>
MAIL_DRIVER=smtp<br>
MAIL_HOST=yoursmptserver.com<br>
MAIL_PORT=465<br>
MAIL_USERNAME=your@email.com<br>
MAIL_PASSWORD=yourpassword<br>
MAIL_ENCRYPTION=ssl<br>
MAIL_FROM_ADDRESS=your@email.com<br>
MAIL_FROM_NAME=yourname<br>

# Use Backup Manager
- Open config/backup.php / go to 133 line and update your e-mail address.
- Open config/database.php / go to 49 line and update dump_binary_path with your correct path.
- Run command "php artisan backup:run" or use backup manager inside admin panel.

# License
- Absolutely Open Source / Free to use!

# Support
If you have any questions you can send an email to: wrzeszczdamian@gmail.com

Best regards
Damian Wrzeszcz
