<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSetupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setups', function (Blueprint $table) {
            $table->bigIncrements('sid');
            $table->string('image')->nullable();
            $table->string('meta_title');
            $table->string('address');
            $table->string('contact');
            $table->string('email');
            $table->string('social')->nullable();
            $table->longText('keywords')->nullable();
            $table->longText('description')->nullable();
            $table->string('gacode')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setups');
    }
}
