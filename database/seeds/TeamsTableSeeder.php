<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teams')->insert([
            'name' => "Tom Rensed",
            'designation' => "Chief Executive Officer",
            'image' => "20012693914team_pic1.jpg",
            'intro' => "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin consequat sollicitudin cursus. Dolor sit amet, consectetur adipiscing elit proin consequat.</p>",
            'social' => "https://www.pinterest.com,https://www.facebook.com,https://www.twitter.com,https://www.linkedin.com",
            'status' => "on",
        ]);

        DB::table('teams')->insert([
            'name' => "Kathren Mory",
            'designation' => "Vice President",
            'image' => "200126101309team_pic2.jpg",
            'intro' => "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin consequat sollicitudin cursus. Dolor sit amet, consectetur adipiscing elit proin consequat.</p>",
            'social' => "https://www.facebook.com,https://www.youtube.com,https://www.linkedin.com,https://www.twitter.com",
            'status' => "on",
        ]);

        DB::table('teams')->insert([
            'name' => "Lancer Jack",
            'designation' => "Senior Manager",
            'image' => "200126104902team_pic3.jpg",
            'intro' => "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin consequat sollicitudin cursus. Dolor sit amet, consectetur adipiscing elit proin consequat.</p>",
            'social' => "https://www.facebook.com,https://www.twitter.com,https://www.linkedin.com,https://www.youtube.com",
            'status' => "on",
        ]);

    }
}
