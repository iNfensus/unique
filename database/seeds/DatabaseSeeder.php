<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
            CategoriesTableSeeder::class,
            ClientsTableSeeder::class,
            ContentsTableSeeder::class,
            PortcatsTableSeeder::class,
            PortfoliosTableSeeder::class,
            ServicesTableSeeder::class,
            SetupsTableSeeder::class,
            TeamsTableSeeder::class,
            TasksTableSeeder::class,
        ]);
    }
}
