<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contents')->insert([
            'title' => "WE CREATE AWESOME WEB TEMPLATES",
            'slug' => "we-create-awesome-web-templates",
            'description' => "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text printer took a galley of type and scrambled it to make a type specimen.</p>",
            'link' => "#",
            'image' => "20012641400main_device_image.png",
            'category' => "home",
            'status' => "on",
        ]);

        DB::table('contents')->insert([
            'title' => "ABOUT US",
            'slug' => "about-us",
            'description' => "<h3>Lorem Ipsum has been the industry&#39;s standard dummy text ever..</h3><p>&nbsp;</p><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.PageMaker including versions of Lorem Ipsum.</p><p>&nbsp;</p><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged like Aldus PageMaker including versions of Lorem Ipsum.</p><p>&nbsp;</p><h4>Want to know more..</h4>",
            'link' => "#contact",
            'image' => "20012641435about-img.jpg",
            'category' => "about us",
            'status' => "on",
        ]);

    }
}
