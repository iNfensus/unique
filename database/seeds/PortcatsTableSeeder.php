<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PortcatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('portcats')->insert([
            'title' => "project",
            'slug' => "project",
            'status' => "on",
        ]);

        DB::table('portcats')->insert([
            'title' => "design",
            'slug' => "design",
            'status' => "on",
        ]);

        DB::table('portcats')->insert([
            'title' => "android",
            'slug' => "android",
            'status' => "on",
        ]);

        DB::table('portcats')->insert([
            'title' => "apple ios",
            'slug' => "apple-ios",
            'status' => "on",
        ]);

        DB::table('portcats')->insert([
            'title' => "web app",
            'slug' => "web-app",
            'status' => "on",
        ]);

    }
}
