<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->insert([
            'title' => "Android",
            'slug' => "android",
            'icon' => "android",
            'description' => "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text.</p>",
            'status' => "on",
        ]);

        DB::table('services')->insert([
            'title' => "Apple IOS",
            'slug' => "apple-ios",
            'icon' => "apple",
            'description' => "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text.</p>",
            'status' => "on",
        ]);

        DB::table('services')->insert([
            'title' => "Design",
            'slug' => "design",
            'icon' => "html5",
            'description' => "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text.</p>",
            'status' => "on",
        ]);

        DB::table('services')->insert([
            'title' => "Concept",
            'slug' => "concept",
            'icon' => "dropbox",
            'description' => "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text.</p>",
            'status' => "on",
        ]);

        DB::table('services')->insert([
            'title' => "User Research",
            'slug' => "user-research",
            'icon' => "slack",
            'description' => "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text.</p>",
            'status' => "on",
        ]);

        DB::table('services')->insert([
            'title' => "User Experience",
            'slug' => "user-experience",
            'icon' => "users",
            'description' => "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text.</p>",
            'status' => "on",
        ]);
    }
}
