<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
           'title' => "home",
           'slug' => "home",
           'status' => "on",
        ]);

        DB::table('categories')->insert([
            'title' => "about us",
            'slug' => "about-us",
            'status' => "on",
        ]);

        DB::table('categories')->insert([
            'title' => "services",
            'slug' => "services",
            'status' => "on",
        ]);

        DB::table('categories')->insert([
            'title' => "portfolio",
            'slug' => "portfolio",
            'status' => "on",
        ]);

        DB::table('categories')->insert([
            'title' => "clients",
            'slug' => "clients",
            'status' => "on",
        ]);

        DB::table('categories')->insert([
            'title' => "team",
            'slug' => "team",
            'status' => "on",
        ]);
    }
}
