<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SetupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('setups')->insert([
            'image' => "200129111321logo.png",
            'meta_title' => "UNIQUE WEBSITE",
            'address' => "104, Some street, NewYork, USA",
            'contact' => "+1 234 567890",
            'email' => "support@sitename.com",
            'social' => "https://www.facebook.com,https://www.twitter.com,https://www.youtube.com,https://www.linkedin.com,https://www.pinterest.com",
            'keywords' => "unique, website, design, project, web development, www, website creation, graphic design, web design, php, laravel, html5, website design, web developer, web designer, web application development",
            'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text printer took a galley of type and scrambled it to make a type specimen.",
            'gacode' => "UA-XXXXXXXX-1",
        ]);
    }
}
