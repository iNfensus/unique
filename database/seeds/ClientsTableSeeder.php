<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->insert([
            'image' => "20012290134client_logo3.png",
            'link' => "#",
            'status' => "on",
        ]);

        DB::table('clients')->insert([
            'image' => "20012290521client_logo5.png",
            'link' => "#",
            'status' => "on",
        ]);

        DB::table('clients')->insert([
            'image' => "20012290528client_logo2.png",
            'link' => "#",
            'status' => "on",
        ]);

        DB::table('clients')->insert([
            'image' => "200122112204client_logo1.png",
            'link' => "#",
            'status' => "on",
        ]);
    }
}
