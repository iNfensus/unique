<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PortfoliosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('portfolios')->insert([
            'title' => "apps",
            'slug' => "apps",
            'category' => "design",
            'image' => "20011973612portfolio_pic4.jpg",
            'status' => "on",
        ]);

        DB::table('portfolios')->insert([
            'title' => "concept",
            'slug' => "concept",
            'category' => "project",
            'image' => "20011973711portfolio_pic3.jpg",
            'status' => "on",
        ]);

        DB::table('portfolios')->insert([
            'title' => "application",
            'slug' => "application",
            'category' => "android",
            'image' => "20011973742portfolio_pic6.jpg",
            'status' => "on",
        ]);

        DB::table('portfolios')->insert([
            'title' => "managment",
            'slug' => "managment",
            'category' => "design",
            'image' => "20011973819portfolio_pic8.jpg",
            'status' => "on",
        ]);

        DB::table('portfolios')->insert([
            'title' => "nexus",
            'slug' => "nexus",
            'category' => "design",
            'image' => "20011973846portfolio_pic5.jpg",
            'status' => "on",
        ]);

        DB::table('portfolios')->insert([
            'title' => "shopping",
            'slug' => "shopping",
            'category' => "android",
            'image' => "20011973925portfolio_pic2.jpg",
            'status' => "on",
        ]);

        DB::table('portfolios')->insert([
            'title' => "features",
            'slug' => "features",
            'category' => "android",
            'image' => "20011973958portfolio_pic1.jpg",
            'status' => "on",
        ]);

        DB::table('portfolios')->insert([
            'title' => "technology",
            'slug' => "technology",
            'category' => "apple ios",
            'image' => "20011974049portfolio_pic8.jpg",
            'status' => "on",
        ]);
    }
}
