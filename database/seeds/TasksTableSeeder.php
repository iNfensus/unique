<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert([
            'name' => "Task one",
            'todo' => "0",
        ]);

        DB::table('tasks')->insert([
            'name' => "Task second",
            'todo' => "0",
        ]);

        DB::table('tasks')->insert([
            'name' => "Task third",
            'todo' => "0",
        ]);

        DB::table('tasks')->insert([
            'name' => "Task fourth",
            'todo' => "0",
        ]);
    }
}
